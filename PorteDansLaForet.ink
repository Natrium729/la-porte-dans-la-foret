# title: La Porte dans la forêt
# author: Nathanaël Marion


/* Cette variable est incrémentée à chaque fois que le joueur fait un choix courageux. L’idée était que le joueur ne pouvait ouvrir la porte que s’il n’avait fait que les choix courageux. Finalement, j’ai trouvé ça pas assez logique ; j’imagine aussi que beaucoup de joueurs auraient raté des fins car il n’auraient pas fait tous les choix courageux et n’auraient pas remarqué qu’il existait une option cachée.
Du coup, j’ai laissé tombé. J’ai quand même gardé la variable, en souvenir et pour « récompenser » les gens curieux qui vont regarder la source.
Alors, toi qui lis ça, félicitations, tu viens de trouver un secret ! *ta-di-da-di-di-da* */
VAR courage = 0

// Comme la Partim 500 n’autorise que 500 mots, j’utilise des variables pour ne pas avoir à répéter certains passages.
VAR description_porte = "Une porte se dresse devant vous. Juste une porte, seule, sans bâtiment autour du chambranle. Pourtant, un rai de lumière s’échappe du cadre. Mais comment est-ce possible ?"
VAR ouverture_porte = "Derrière, aussi invraisemblable que cela puisse paraître, s’alignent des dizaines d’étagères."

-> introduction

=== introduction ===

« Je t’avais dit que ce n’était pas une bonne idée ! » reprochez-vous à Aleth. Elle fait mine de ne pas vous entendre et disparaît derrière un arbre.

	*	Vous essayez de la rattraper.
		-> suivre
	*	Vous continuez à votre rythme.
		~ courage++
		-> seul

=== suivre ===

« Eh, Aleth, pas si vite ! »

Vous maudissez votre décision de la suivre. Cette histoire de raccourci sentait les problèmes depuis le début.

Vous entendez un craquement un peu plus loin.

	*	C’est sûrement Aleth.
		Vous vous approchez prudemment. Votre amie se tient dans une éclaircie, quelques mètres devant vous.
		~ courage++
	*	Aleth est partie de l’autre côté. Inutile de perdre votre temps.
		Vous vous retournez quand une voix dans votre dos vous fait sursauter.

-	« Chut ! fait Aleth. Viens voir ! »

Vous vous approchez d’elle et vous mettez à ses côtés.

{description_porte}

Vous devinez les pensées qui traversent l’esprit de votre amie.

« On entre ? propose-t-elle.

	*	— Tu es folle ?[] Nous avons déjà perdu trop de temps, nos parents vont s’inquiéter. »
		Vous prenez Aleth par la main et la forcez à vous éloigner. Peu de temps plus tard, vous émergez des arbres et finissez par retrouver vos familles.
		Avant de vous quitter, Aleth vous lance un regard accusateur. Et bien que vous ne veuillez pas l’admettre, il est vrai que vous avez comme l’impression d’avoir gâché quelque chose.
		Vous secouez la tête pour chasser ces pensées. Vous êtes indemnes, c’est ce qui compte.
	*	— Pourquoi pas ?[] » dites-vous malgré vous.
		Vous avancez, jusqu’à n’être plus qu’à quelques centimètres du battant. Aleth pose la main sur la poignée et ouvre la porte.
	 	{ouverture_porte} Vous vous tenez par la main et franchissez le seuil.

-	-> END

=== seul ===

Il commence à faire sombre et la forêt prend des airs lugubres, mais vous n’avez pas trop peur. Et puis, votre amie n’ira pas bien loin sans vous. C’est une véritable tête de mule, mais elle ne vous laisserait pas tomber. Sûrement.

Un grincement s’élève dans l’air immobile. Comme une vieille porte qui s’ouvre. En pleine forêt.

	*	Vous voulez en savoir plus.
		~ courage++
	*	Votre imagination vous joue des tours.[] Le plus important est de ne pas trop vous laisser distancer par Aleth.

-	Vous n’avez pas fait trois pas qu’un cri retentit. Aleth !

Il venait de la même direction que le grincement. Pas de temps à perdre ! Ce ne sera pas vous qui la laisserez tomber !

Vous arrivez enfin. {description_porte}

Vous vous approchez d’elle. Elle est entrouverte. Aleth aurait-elle… Ridicule ; la porte ne mène nulle part.

	*	Vous l’ouvrez.
		{ouverture_porte}
		« Je te retrouverai, Aleth, » pensez-vous en franchissant le seuil.
	*	Vous vous détournez.
		Votre amie est introuvable et vous finissez par rejoindre vos parents. Vous leur apprenez la nouvelle. Ils sont dévastés ; Aleth ne sera jamais revue.

-	-> END